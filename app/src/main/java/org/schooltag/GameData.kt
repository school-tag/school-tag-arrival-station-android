package org.schooltag

import com.google.firebase.database.IgnoreExtraProperties
import kotlinx.serialization.Serializable
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.json.Json
import java.text.SimpleDateFormat
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*

/** NFC Scan at a Remote Way Station that reported time, station identity and vitals.
 * TagId is omitted since the visit resides on a tag that has identity.
 * https://bitbucket.org/school-tag/school-tag-station-common/src/d791243cb68c1cff2740aafaa4dad3cf4496239d/src/TagIo.h#lines-75
 *
 * @property stationId uniquely identifying the station within the scope of a game
 * @property timestamp unix time indicating the moment the station scanned the tag
 * @property batteryLevel indicating if a recharge may be necessary
 * @property version of the software on the station
 */
data class StationVisit(
    val stationId:Short,
    val timestamp:Int,
    val batteryLevel: BatteryLevel,
    val version: Byte,
)


/**Level indicator for battery voltage for the station*/
enum class BatteryLevel { recharge,low,medium,high}

/** A record of a single Visit used to communicate with the cloud. Ready to become JSON
 * for submission as a pubsub message.
 *
 * @property tagId NFC UID of the School Tag
 * @property station unique ID of the station
 * @property timestamp ISO timestamp in UTC
 * @property batteryLevel string equivalent of BatteryLevel enum
 * @property version of the software running the station
 */
@Serializable
data class Scan(val tagId:String,
                val station:String,
                val timestamp: String,
                val batteryLevel: String,
                val version: String,
)

/** Represents the School Tag Scanned, but has no identity */
@IgnoreExtraProperties
data class Tag(val id: String, val userId:String){
    constructor() : this("", "")
}

/** Identifies the player that owns the school tag */
@IgnoreExtraProperties
data class User(val id: String?, val nickname:String?, val avatar:String?, ){
    constructor() : this("", "","")
}

/** Represents a single reward given for a scan of a tag */
@IgnoreExtraProperties
data class UserReward(
    val id:String,
    val points:Short,
    val coins:Short?,
    val metersToSchool:Int,
    val ruleId:String,
    val timeReceived: String,
)

/**
 * Summary of rewards earned for a single time quantity encoded in the path.
 * @property path leading to this score in the database
 * @property pointsForRules map keyed by rule name with total points earned for time period
 */
@IgnoreExtraProperties
data class Score(
    val path:String,
    val points:Short,
    val coins:Short?,
    val metersToSchool: Int,
    val userId: String?,
    val pointsForRules: Map<String,Int>,
)

/** Converts a station visit to a cloud ready scan.
 * @param tagId of the tag from where the visit was read
 * @param visit of the details of the station scan
 */
fun visitToScan(tagId:String,visit:StationVisit):Scan{
    val date = Date(visit.timestamp.toLong() * 1000)
    val timeString = timestampString(date)
    return Scan(
        tagId=tagId,
        station = visit.stationId.toString(),
        timestamp = timeString,
        batteryLevel = visit.batteryLevel.toString(),
        version = visit.version.toString()
    )
}

/**Creates a scan for this station*/
fun scannedHereAndNow(tagId:String, stationId:String,batteryLevel: BatteryLevel,version: String):Scan{
    val timeString = timestampString(Date())
    return Scan(
        tagId = tagId,
        timestamp = timeString,
        station = stationId,
        batteryLevel = batteryLevel.toString(),
        version = version)
}
/**Formats a date into an ISO portable date/time.*/
fun timestampString(date:Date):String{
    val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
    formatter.timeZone = TimeZone.getTimeZone("UTC");
    return formatter.format(date)
}
/** converts the given list of visits to scans ready to send*/
fun visitsToScans(tagId: String, visits:List<StationVisit>):List<Scan>{
    return visits.map { visit -> visitToScan(tagId,visit) }
}

/**Converts the scans into json ready to send to the cloud*/
fun scansJsonFromVisits(tagId: String,visits: List<StationVisit>):String{
    val scans = visitsToScans(tagId,visits)
    return scansJson(scans)
}

fun scansJson(scans:List<Scan>):String{
    return Json.encodeToString(ListSerializer(Scan.serializer()),scans)
}