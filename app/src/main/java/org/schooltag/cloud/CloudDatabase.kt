package org.schooltag.cloud

import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import org.schooltag.Tag
import org.schooltag.User
import java.io.FileNotFoundException
import java.lang.Exception
import kotlin.coroutines.coroutineContext
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine
import kotlin.jvm.Throws

/**Firebase communications to the cloud database to retrieve GameData*/
class CloudDatabase(projectId:String) {

    private val database = Firebase.database("https://$projectId.firebaseio.com/")

    @Throws(DataNotFoundException::class)
    suspend fun tag(tagId:String): Tag  {
       return fetch("/tags/$tagId",Tag::class.java)
    }

    @Throws(DataNotFoundException::class)
    suspend fun user(userId:String): User{
        return fetch("/users/$userId",User::class.java)
    }
 
    @Throws(DataNotFoundException::class)
    suspend fun userForTag(tagId: String):User{
        return user(tag(tagId).userId)
    }

    @Throws(DataNotFoundException::class)
    private suspend fun <T>fetch(path:String,type:Class<T>) = suspendCoroutine<T> {
        database.getReference(path).addValueEventListener(object:ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                val value=snapshot.getValue(type)
                if(value != null) {
                    it.resume(value)
                }else{
                    it.resumeWithException(DataNotFoundException(path))
                }
            }

            override fun onCancelled(error: DatabaseError) {
                it.resumeWithException((error.toException()))
            }
        })
    }
}

/**Thrown when Firebase returns null indicating the entity requested is not present.*/
class DataNotFoundException(path:String):Exception("Data not found at $path")