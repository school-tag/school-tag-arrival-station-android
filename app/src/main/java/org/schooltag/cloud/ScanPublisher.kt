package org.schooltag.cloud

import com.google.api.core.ApiFuture
import com.google.api.gax.core.FixedCredentialsProvider
import com.google.auth.oauth2.GoogleCredentials
import com.google.cloud.pubsub.v1.Publisher
import com.google.protobuf.ByteString
import com.google.pubsub.v1.ProjectTopicName
import com.google.pubsub.v1.PubsubMessage
import org.schooltag.Scan
import java.io.InputStream

class ScanPublisher(val jsonCredentials: InputStream?, val projectName: String){
    private val topicName="stations-scans"
    private val credentials  = GoogleCredentials.fromStream(jsonCredentials)
    private val projectTopicName: ProjectTopicName = ProjectTopicName.of(projectName, topicName);

    private val publisher: Publisher = Publisher.newBuilder(projectTopicName).setCredentialsProvider(
        FixedCredentialsProvider.create(credentials)).build()

    fun publishStationScans(message:String):ApiFuture<String>{
        val data = ByteString.copyFromUtf8(message)
        val pubsubMessage = PubsubMessage.newBuilder().setData(data).build()
        return publisher.publish(pubsubMessage)
    }
}