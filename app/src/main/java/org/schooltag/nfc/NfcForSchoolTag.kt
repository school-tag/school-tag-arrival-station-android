package org.schooltag.nfc

import android.nfc.FormatException
import android.nfc.NdefMessage
import android.nfc.NdefRecord
import android.nfc.tech.MifareUltralight
import android.util.Log
import com.google.common.collect.ImmutableList
import org.schooltag.BatteryLevel
import org.schooltag.StationVisit
import java.io.IOException
import java.nio.ByteBuffer
import java.util.*
import kotlin.math.max


const val VISIT_FORMAT_VERSION_SUPPORTED:Byte = 0xBC.toByte()
const val OFFSET_VERSION =0;
const val OFFSET_VISIT_COUNT=2
const val OFFSET_VISIT_DATA=22

/** NFC Details specifically related to School Tags decoding remote stations, etc.*/
class NfcForSchoolTag {
    companion object{
        fun uriMessage( tagId:String): NdefMessage {
            val message: NdefMessage
            val urlRecord = NdefRecord.createUri("https://schooltag.org?tag=$tagId")
            message = NdefMessage(urlRecord)
            return message
        }

        /**
         * Fixed number of pages to read in the Mifare Ultralight tag to retrieve all possible
         * data. The header declares the number of visits recorded, but we'll read all
         * of the pages possible and process the data in bulk, even if extra reads happen.
         */
        fun numberOfMifareUltralightPagesToRead():Int{

            val pageSize = MifareUltralight.PAGE_SIZE
            val numberOfBytesPerVisit = 8
            val maxNumberOfVisits=5
            val pagesPerVisit = numberOfBytesPerVisit / pageSize
            val numberOfPagesForVisits = maxNumberOfVisits * pagesPerVisit
            val numberOfPagesForHeader = 1
            return numberOfPagesForHeader + numberOfPagesForVisits
        }

        /** Reads the pages of bytes where visit data is stored on the tag.
         * Returns the visits found, if any.
         * IO exceptions indicate a retry may produce results.
         *
         * See TagIo for the documented format
         * https://bitbucket.org/school-tag/school-tag-station-common/src/1.2.1/src/TagIo.h
         */
        @Throws(IOException::class, FormatException::class)
        fun stationVisits(tag:NfcWritableTag):List<StationVisit>{
            val startPage= OFFSET_VISIT_DATA
            val visitBytes = tag.readPages(startPage, numberOfMifareUltralightPagesToRead())
            return stationVisits(visitBytes)
        }

        @Throws(UnknownFormatConversionException::class)
        fun stationVisits(visitBytes:ByteBuffer):List<StationVisit>{
            val version = visitBytes.get(0)
            val visits = mutableListOf<StationVisit>()

            if(version == VISIT_FORMAT_VERSION_SUPPORTED){
                val numberOfVisits = visitBytes.get(2)
                for(visitIndex in 0 until numberOfVisits){
                    val offset = 4 + (visitIndex * 8)
                    val stationId = visitBytes.getShort(offset)
                    val vitals = visitBytes.getShort(offset + 2)
                    val timestamp = visitBytes.getInt(offset + 4)
                    val visit = StationVisit(stationId,timestamp,BatteryLevel.medium,version)
                    visits.add(visit)
                }
            } else {
                throw UnknownFormatConversionException("unhandled version $version")
            }

            return visits
        }

        /**Writes a visit header indicating zero visits, essentially erasing the visits */
        fun resetVisits(tag:NfcWritableTag){
            tag.writePage(OFFSET_VISIT_DATA,noVisitsHeader())
        }
        /**Header with zero for visit count, indicating visits are not present.*/
        fun noVisitsHeader():ByteBuffer{
            var bytes = ByteBuffer.allocate(4)
            bytes.put(OFFSET_VERSION, VISIT_FORMAT_VERSION_SUPPORTED)
            bytes.put(OFFSET_VISIT_COUNT,0)
            return bytes
        }
    }


}