package org.schooltag.nfc

import android.content.Context
import android.content.Intent
import android.nfc.NdefMessage
import android.nfc.NdefRecord
import android.nfc.NfcAdapter
import android.nfc.Tag
import android.os.Build
import androidx.annotation.RequiresApi
import com.google.common.io.BaseEncoding

/**General helpers interacting with NFC Tags */
class NfcUtils {
    companion object {
        fun getUID(intent: Intent): String {
            val myTag = intent.getParcelableExtra<Tag>(NfcAdapter.EXTRA_TAG)
            return BaseEncoding.base16().encode(myTag.id)
        }


    }
}