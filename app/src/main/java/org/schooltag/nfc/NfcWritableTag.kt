package org.schooltag.nfc

import android.net.Uri
import android.nfc.FormatException
import android.nfc.NdefMessage
import android.nfc.Tag
import android.nfc.tech.Ndef
import android.nfc.tech.NdefFormatable
import android.nfc.tech.MifareUltralight
import android.util.Log
import org.schooltag.station.ByteUtils
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.nio.ByteBuffer

class NfcWritableTag @Throws(FormatException::class) constructor(tag: Tag) {
    private val NDEF = Ndef::class.java.canonicalName
    private val NDEF_FORMATABLE = NdefFormatable::class.java.canonicalName
    private val MIFARE_ULTRALIGHT = MifareUltralight::class.java.canonicalName
    /** Formatted in NDEF already, likely contains school tag url */
    private val ndef: Ndef?
    /** Not already formatted, but we'll format it and claim it as ours */
    private val ndefFormatable: NdefFormatable?
    /** used to write bytes after the ndef message in custom school tag format */
    private val mifareUltralight: MifareUltralight
    private val logTag = "NfcWriteableTag"
    /**The tag id from the tag */
    val tagId:String


    init {
        val technologies = tag.techList
        val tagTechs = listOf(*technologies)

        mifareUltralight = if(tagTechs.contains(MIFARE_ULTRALIGHT)){
            Log.i(logTag, "contains mifare ultralight")
            MifareUltralight.get(tag)
        }else{
            throw FormatException("Tag is not mifare ultralight")
        }

        when {
            tagTechs.contains(NDEF) -> {
                Log.i(logTag, "contains ndef")
                ndef = Ndef.get(tag)
                ndefFormatable = null
                tagId= bytesToHexString(ndef.tag.id)
            }
            tagTechs.contains(NDEF_FORMATABLE) -> {
                Log.i(logTag, "contains ndef_formatable")
                ndefFormatable = NdefFormatable.get(tag)
                tagId= bytesToHexString(ndefFormatable.tag.id)
                ndef = null
            }
            else -> {
                throw FormatException("Tag doesn't support ndef")
            }
        }

    }


    /** finds the first URI record, if any */
    @Throws(IOException::class, FormatException::class)
    fun readUri(): Uri?{
        if (ndef != null){
            if(!ndef.isConnected) {
                ndef.connect()
            }
            ndef.use { ndef ->
                val records = ndef.ndefMessage.records
                for (record in records) {
                    val uri = record.toUri()
                    if(uri != null){
                        return uri
                    }
                }
            }
        }
        return null
    }

    /** Reads mifare pages of data starting at the address offset and return the number of pages
     * requested.
     */
    @Throws(IOException::class, FormatException::class)
    fun readPages(start:Int,numberOfPages:Int):ByteBuffer{
        val pagesPerRead = 4
        val numberOfReads = Math.ceil(numberOfPages.toDouble() / pagesPerRead).toInt()
        val capacity = numberOfReads * pagesPerRead * MifareUltralight.PAGE_SIZE
        val bytes = ByteBuffer.allocate(capacity)

        if(!mifareUltralight.isConnected){
            mifareUltralight.connect()
        }

        mifareUltralight.use { mifareUltralight ->
            for (whichRead in 0 until numberOfReads){
                val readBytes = mifareUltralight.readPages(start + (whichRead * MifareUltralight.PAGE_SIZE))
                bytes.put(readBytes)
            }
        }

        return bytes
    }

    /**Write a single page, starting at the index given.*/
    @Throws(IOException::class)
    fun writePage(start:Int, bytes: ByteBuffer){
        if(!mifareUltralight.isConnected){
            mifareUltralight.connect()
        }
        mifareUltralight.use {
            mifareUltralight.writePage(start,bytes.array())
        }
    }

    @Throws(IOException::class, FormatException::class)
    fun writeData(tagId: String,
                  message: NdefMessage): Boolean {
        if (tagId != tagId) {
            return false
        }
        if (ndef != null && ndef.isWritable) {
            if(!ndef.isConnected) {
                ndef.connect()
            }
            if (ndef.isConnected) {
                ndef.use { ndef.writeNdefMessage(message) }
                return true
            }
        } else if (ndefFormatable != null) {
            ndefFormatable.connect()
            if (ndefFormatable.isConnected) {
                ndefFormatable.use { ndefFormatable.format(message) }
                return true
            }
        }
        return false
    }

    companion object {
        fun bytesToHexString(src: ByteArray): String {
            if (ByteUtils.isNullOrEmpty(src)) {
                throw FormatException("cannot read tag id")
            }
            val sb = StringBuilder()
            for (b in src) {
                sb.append(String.format("%02X", b))
            }
            return sb.toString()
        }
    }
}