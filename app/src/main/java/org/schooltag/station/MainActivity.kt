package org.schooltag.station

import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.net.Uri
import android.nfc.FormatException
import android.nfc.NfcAdapter
import android.nfc.Tag
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.api.core.ApiFutureCallback
import com.google.api.core.ApiFutures
import com.google.common.util.concurrent.MoreExecutors
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import org.schooltag.*
import org.schooltag.cloud.CloudDatabase
import org.schooltag.cloud.ScanPublisher
import org.schooltag.nfc.NfcForSchoolTag
import org.schooltag.nfc.NfcWritableTag
import org.schooltag.station.ui.main.MainFragment
import java.io.FileNotFoundException
import java.io.IOException
import java.util.*
import kotlin.coroutines.suspendCoroutine


class MainActivity : AppCompatActivity() {

    private val logTag = "MainActivity"

    private var previousIntent: Intent? = null
    private var pubSubService: ScanPublisher? = null
    private var stationId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d(logTag, "created")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, MainFragment.newInstance())
                    .commitNow()
        }
        assignStationId()
        handleNfcIntent(intent)
    }

    override fun onResume() {
        super.onResume()
        //https://stackoverflow.com/a/36942185 provided the insight for handling this way
        //always called after onNewIntent and onCreate allowing a tag reward for a closed app
        handleNfcIntent(intent)
        //clear the intent once it is handled so it won't come back
    }

    private fun assignStationId(){
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w(logTag, "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            val token = task.result
            stationId = token;
        })

    }
    private fun pubSubService():ScanPublisher{
        if(pubSubService == null){
            val credentialsFile = resources.openRawResource(R.raw.gcloud_service_station_credentials)
            pubSubService = ScanPublisher(credentialsFile, "school-tag-sbox")
            credentialsFile.close()
        }
        return pubSubService!!
    }

    /**When an Intent is received, inspect it for the appropriate NFC scan.  Read and write
     * data if it is a school tag and publish the scans for rewards, showing the rewards page.
     */
    private fun handleNfcIntent(intent: Intent){
        Log.d(logTag, "handling intent $intent")
        if(!intent.filterEquals(previousIntent)){
            Log.d(logTag, "New intent found")
            if(isSupportedIntent(intent)) {
                previousIntent = intent
                val data = intent.data
                Log.d(logTag, "data received: $data")
                val tagFromIntent = intent.getParcelableExtra<Tag>(NfcAdapter.EXTRA_TAG)
                if(tagFromIntent != null){
                   handleTagData(tagFromIntent)
                }
            }else{
                Log.d(logTag, "Intent not supported")
            }
        } else{
            Log.d(logTag, "Avoiding duplicate intent")
        }
    }

    /**Read the data from the tag, send it to the cloud for rewards.
     * Erase the visits from the tag when successfully persisted.
     */
    private fun handleTagData(nfcTag:Tag){
        try {
            val tag = NfcWritableTag(nfcTag)
            showScoreboard(tag.tagId)
            showStationVisitCount(readAndPublishVisits(tag))
            writeNDefMessage(tag)

        } catch (e: IOException){
            Log.e(logTag, "Error communicating", e)
        } catch (e: FormatException) {
            Log.e(logTag, "Unsupported tag tapped", e)
            return
        }
    }

    /**
     * Read the visits from the station, then publish to the cloud
     */
    private fun readAndPublishVisits(tag: NfcWritableTag):Int {
        try {
            val visits = NfcForSchoolTag.stationVisits(tag)
            Log.d(logTag, "visits $visits")
            publishVisits(tag.tagId, visits)
            return visits.size
        } catch (e: UnknownFormatConversionException) {
            Log.d(logTag, "station version not recognized, which is expected for new tags")
        }
        return 0
    }

    /**Display the scoreboard for the tag.*/
    private fun showScoreboard(tagId: String){
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://sandbox.schooltag.org/tag/$tagId"))
        startActivity(browserIntent)
    }

    /**
     * Display the number of visits read from the tag, if any.
     */
    private fun showStationVisitCount(count:Int){
        if (count > 0) {
            val message = resources.getQuantityString(
                R.plurals.way_station_message,
                count,
                count
            )
        }
    }
    /**Sending the visits as scans to be rewarded, including the scan at this station.*/
    private fun publishVisits(tagId: String, visits: List<StationVisit>){
        //FIXME: this needs to be a callback
        if(stationId == null) {
            stationId = "aaaaaaaaa"
            Log.w(logTag, "Station ID is not yet assigned")
        }
        var version = "?"

        try {
            val pInfo: PackageInfo = applicationContext.packageManager.getPackageInfo(applicationContext.packageName, 0)
            version = pInfo.versionName
        } catch (e: PackageManager.NameNotFoundException) {
           Log.w(logTag,"Problem getting app version",e)
        }

        val arrivalScan = scannedHereAndNow(
            tagId = tagId,
            stationId = stationId!!,
            batteryLevel = BatteryLevel.high,
            version=version)
        val visitsScans = visitsToScans(tagId,visits)
        val scans = visitsScans + arrivalScan
        val scanMessage = scansJson(scans)

        val future = pubSubService().publishStationScans(scanMessage)
        ApiFutures.addCallback(
            future,
            object : ApiFutureCallback<String> {
                override fun onSuccess(messageId: String) {
                    Log.d(logTag, "published with message id: $messageId")
                }

                override fun onFailure(t: Throwable) {
                    Log.e(logTag, "failed to publish:", t)
                }
            },
            MoreExecutors.directExecutor()
        )

    }

    @Throws(IOException::class, FormatException::class)
    private fun writeNDefMessage(tag: NfcWritableTag) {
        val tagId = tag.tagId
        val message = NfcForSchoolTag.uriMessage(tagId)
        var writeSuccess = false
        try {
            writeSuccess = tag.writeData(tagId,message)
            if(writeSuccess){
                //throws IO Exception if failed
                NfcForSchoolTag.resetVisits(tag)
            }
        }catch (io: IOException){
            Log.e(logTag, "Failed to write", io)
        }catch (format: FormatException){
            Log.e(logTag, "Failed to write", format)
        }
        if(!writeSuccess){
            Toast.makeText(this,"Failed to write tag",Toast.LENGTH_LONG).show()
        }
    }

    /** Checks if it is an NDEF event.  No need to check for proper URL since that is embedded in
     * manifest.
     */
    private fun isSupportedIntent(intent: Intent?): Boolean {
        return if (intent != null) {
            val action = intent.action
            NfcAdapter.ACTION_NDEF_DISCOVERED == action || NfcAdapter.ACTION_TECH_DISCOVERED == action
        } else {
            false
        }
    }




}