package org.schooltag.station

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.appcompat.app.AppCompatActivity

const val TAG_ID_KEY="org.schooltag.tagId"
const val logTag = "UserRewardsActivity"
class UserRewardsActivity : AppCompatActivity() {
    companion object{
        fun tagId(tagId: String, intent: Intent){
            intent.putExtra(TAG_ID_KEY, tagId)
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_rewards)
        val tagId = intent.getStringExtra(TAG_ID_KEY)
        Log.d(logTag, "Showing tag id $tagId")

        val finishTime:Long = 10 * 1000 //10 secs

        val handler = Handler()
        handler.postDelayed(Runnable { this@UserRewardsActivity.finish() }, finishTime)
    }

}