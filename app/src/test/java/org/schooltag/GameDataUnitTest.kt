package org.schooltag

import org.junit.Test

import org.junit.Assert.*
import org.schooltag.nfc.*
import java.nio.ByteBuffer
import java.util.*


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class GameDataUnitTest {


    @Test
    fun visitToScan_mapsSuccessfully() {
        val tagId = TAG_ID_1
        val visit = station1Visit()
        val scan = visitToScan(tagId,visit)
        assertEquals(tagId,scan.tagId)
        assertEquals(STATION_1_ID.toString(),scan.station)
        assertEquals(STATION_1_TIMESTAMP_ISO,scan.timestamp)
        assertEquals(STATION_1_BATTERY_LEVEL.toString(),scan.batteryLevel)
        assertEquals(STATION_1_VERSION.toString(),scan.version)
    }

    @Test
    fun visitsToJson(){
        val visits = listOf(station1Visit())
        val scansJson = scansJsonFromVisits(tagId = TAG_ID_1,visits)
        val exptectedJon =
            """[{"tagId":"ABC234","station":"-21829","timestamp":"1979-02-10T00:20:20Z","batteryLevel":"medium","version":"3"}]"""
        assertEquals(exptectedJon,scansJson)
    }
}