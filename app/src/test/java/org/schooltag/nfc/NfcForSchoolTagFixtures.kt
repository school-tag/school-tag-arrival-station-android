package org.schooltag.nfc

import org.schooltag.BatteryLevel
import org.schooltag.StationVisit
import java.nio.ByteBuffer


fun validHeader(visitCount:Int):ByteBuffer{
    val bytes = ByteBuffer.allocate(4 + 8 * visitCount +4)
    bytes.put(OFFSET_VERSION, VISIT_FORMAT_VERSION_SUPPORTED)
    bytes.put(OFFSET_VISIT_COUNT,visitCount.toByte())
    return bytes
}

const val TAG_ID_1 = "ABC234"

const val STATION_1_ID = 0xAABB.toShort()
const val STATION_1_TIMESTAMP = 0x11223344
const val STATION_1_TIMESTAMP_ISO = "1979-02-10T00:20:20Z"
val STATION_1_BATTERY_LEVEL = BatteryLevel.medium
const val STATION_1_VERSION = 3.toByte()
const val STATION_2_ID = 0xCCDD.toShort()
const val STATION_2_TIMESTAMP = 0x55667788

fun station1Visit():StationVisit{
    return StationVisit(STATION_1_ID, STATION_1_TIMESTAMP, STATION_1_BATTERY_LEVEL,
        STATION_1_VERSION)
}
fun station1Visit(bytes: ByteBuffer){
    bytes.putShort(4, STATION_1_ID)
    bytes.putInt(8, STATION_1_TIMESTAMP)
}

fun station2Visit(bytes: ByteBuffer){
    bytes.putShort(12, STATION_2_ID)
    bytes.putInt(16, STATION_2_TIMESTAMP)
}