package org.schooltag.nfc

import org.junit.Test

import org.junit.Assert.*
import org.schooltag.*
import java.nio.ByteBuffer
import java.util.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class NfcForSchoolTagUnitTest {

    @Test
    fun numberOfMifareUltralightPagesToRead_isFixed() {
        assertEquals(11, NfcForSchoolTag.numberOfMifareUltralightPagesToRead())
    }

    @Test(expected = UnknownFormatConversionException::class)
    fun stationVisits_invalidVersionThrowsUnhandledFormatException() {
        val bytes = ByteBuffer.allocate(4)
        bytes.put( (VISIT_FORMAT_VERSION_SUPPORTED + 1).toByte())
        assertEquals(0, NfcForSchoolTag.stationVisits(bytes).size)
    }

    @Test
    fun stationVisits_validVersionNoVisitsReturnsEmpty() {
        val count= 0
        val bytes = validHeader(visitCount = count)
        assertEquals(count, NfcForSchoolTag.stationVisits(bytes).size)
    }

    private fun assertStation1(stationVisits:List<StationVisit>){
        val visit = stationVisits[0]
        assertEquals(STATION_1_ID,visit.stationId)
        assertEquals(STATION_1_TIMESTAMP,visit.timestamp)
    }

    private fun assertStation2(stationVisits:List<StationVisit>){
        val visit = stationVisits[1]
        assertEquals(STATION_2_ID,visit.stationId)
        assertEquals(STATION_2_TIMESTAMP,visit.timestamp)
    }

    @Test
    fun stationVisits_validOneVisit(){
        val count =1
        val bytes = validHeader(visitCount = count)
        station1Visit(bytes)
        val stationVisits = NfcForSchoolTag.stationVisits(bytes)
        assertEquals(count,stationVisits.size)
        assertStation1(stationVisits)
    }

    @Test
    fun stationVisits_validTwoVisits(){
        val count =2
        val bytes = validHeader(visitCount = count)
        station1Visit(bytes)
        station2Visit(bytes)
        val stationVisits = NfcForSchoolTag.stationVisits(bytes)
        assertEquals(count,stationVisits.size)
        assertStation1(stationVisits)
        assertStation2(stationVisits)
    }
}